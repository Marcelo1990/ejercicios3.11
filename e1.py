# Reescribe el programa del cálculo del salario para darle al empleado 1.5 veces la tarifa horaria para todas las horas trabajadas que excedan de 40.
# Angel Marcelo Merchan Infante
# angel.merchan@unl.edu.ec

print ('Bienvenido, llego el momento de calcular un salario:')

htrabajadas = float(input('Ingrese el número de horas trabajadas:'))
thora = float(input('Ingrese la tarifa por hora:'))

reslt = htrabajadas * thora
if htrabajadas > 0  and htrabajadas <= 40:
    print(f'El salario total es de: {reslt:.2f} $')
elif htrabajadas >40:
    hextra = (htrabajadas-40)
    print(f'El salario normal es de: {reslt:.2f} $')
    print ('horas extra: ', hextra)
    print ('salario de horas extra: ', (hextra*5))
    reslt = reslt + (hextra*5)
    print(f'El salario total es de: {reslt:.2f} $')
else:
    print ('Por favor introduzca un número positivo')
